﻿using UnityEngine;
using System.Collections;

public class PlayerSaver : MonoBehaviour
{
    void Start()
    {

    }
    
    public static void NewSave(int slot)
    {
        PlayerPrefs.SetInt("maxhealth_SaveSlot" + slot, 20);
        PlayerPrefs.SetInt("health_SaveSlot" + slot, 20);
        PlayerPrefs.SetFloat("speed_SaveSlot" + slot, 4);
        PlayerPrefs.SetInt("damage_SaveSlot" + slot, 1);
        PlayerPrefs.SetFloat("attackSpeed_SaveSlot" + slot, 1);
        PlayerPrefs.SetString("level_SaveSlot" + slot, "level1");
    }

    public static void SaveStats(VirusStats stats, int slot, string levelName)
    {
        PlayerPrefs.SetInt("maxhealth_SaveSlot" + slot, stats.MaxHealth());
        PlayerPrefs.SetInt("health_SaveSlot" + slot, stats.Health());
        PlayerPrefs.SetFloat("speed_SaveSlot" + slot, stats.Speed());
        PlayerPrefs.SetInt("damage_SaveSlot" + slot, stats.Damage());
        PlayerPrefs.SetFloat("attackSpeed_SaveSlot" + slot, stats.AttackSpeed());
        PlayerPrefs.SetString("level_SaveSlot" + slot, levelName);
    }

    public static string LoadStatsIn(VirusStats stats, int slot)
    {
        stats.setMaxHealth(PlayerPrefs.GetInt("maxhealth_SaveSlot" + slot, 20));
        stats.setHealth(PlayerPrefs.GetInt("health_SaveSlot" + slot, 20));
        stats.setSpeed(PlayerPrefs.GetFloat("speed_SaveSlot" + slot, 4));
        stats.setDamage(PlayerPrefs.GetInt("damage_SaveSlot" + slot, 1));
        stats.setAttackSpeed(PlayerPrefs.GetFloat("attackSpeed_SaveSlot" + slot, 1));
        return PlayerPrefs.GetString("level_SaveSlot" + slot);
    }
}
