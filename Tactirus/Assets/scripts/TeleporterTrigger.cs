﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterTrigger : MonoBehaviour
{
    public GameObject Arrival;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        Vector3 position = Arrival.transform.Find("spawnPos").transform.position;
        other.gameObject.transform.position = position;
    }
}
