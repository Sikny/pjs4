﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorCloser : MonoBehaviour
{
    public Vector3 spawnPoint;
    public AudioClip bossMusic;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            GameObject.Find("boss_door").GetComponent<LockedDoor>().lever.GetComponent<DoorOpener>().changeState();
            GameObject Virus = GameObject.Find("Virus");
            Virus.transform.position = spawnPoint;
            GameObject Boss = GameObject.Find("Avost");
            Boss.GetComponent<BuildFinalBoss>().expand();
            new WaitForSecondsRealtime(1);
            Boss.GetComponent<BossShooter>().focusPlayer();
            GameObject.Find("Audio Source").GetComponent<AudioSource>().clip = bossMusic;
            GameObject.Find("Audio Source").GetComponent<AudioSource>().Play();
            Destroy(this);
        }
    }
}
