﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public float speed;

    void Start()
    {
        transform.localScale = GameObject.Find("Virus").transform.localScale / 3;

        var ray = FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition);

        RaycastHit hit = new RaycastHit();

        int layer_ground = LayerMask.GetMask("Ground");
        if (Physics.Raycast(ray, out hit, 50.0f, layer_ground))
        {
            transform.LookAt(hit.point);
            GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x, 0, transform.forward.z) * speed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Objets autorisés à traverser
        if (!other.gameObject.tag.Equals("Player") && !other.isTrigger)
        {
            Breakable b;
            if(b = other.GetComponent<Breakable>())
            {
                b.takeDmg(GameObject.Find("Virus").GetComponent<VirusStats>().Damage());
            }
            Debug.Log(other.gameObject.name);
            Destroy(this.gameObject);
        }
    }
}
