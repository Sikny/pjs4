﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTrigger : MonoBehaviour
{
    public string nextLevelName;

    void OnTriggerEnter(Collider other) {
        print("Niveau terminé !");
        UnityEngine.SceneManagement.SceneManager.LoadScene(nextLevelName);
    }
}
