﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPrismBuilder : MonoBehaviour {
    Vector3[] newVertices = new Vector3[6]{
        new Vector3(-0.5f, 0, -0.5f), new Vector3(0.5f, 0, -0.5f),
        new Vector3(0.5f, 0, 0.5f), new Vector3(-0.5f, 0, 0.5f),
        new Vector3(0, 3, 0), new Vector3(0, -3, 0)
    };
    Vector2[] newUV = {
        new Vector2(0, 0), new Vector2(1, 0),
        new Vector2(1, 1), new Vector2(0, 1),
        new Vector2(1, 1), new Vector2(1, 1)
    };
    int[] newTriangles = new int[24]{
        0, 4, 1,    0, 3, 4,    1, 4, 2,    2, 4, 3,
        0, 1, 5,    0, 5, 3,    1, 2, 5,    2, 3, 5
    };
    
    // Start is called before the first frame update
    void Start()
    {
        Mesh mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        mesh.vertices = newVertices;
        mesh.uv = newUV;
        mesh.triangles = newTriangles;
        Vector3[] normals = new Vector3[6];

        normals[0] = -Vector3.forward;
        normals[1] = -Vector3.forward;
        normals[2] = -Vector3.forward;
        normals[3] = -Vector3.forward;
        normals[4] = -Vector3.forward;
        normals[5] = -Vector3.forward;

        mesh.normals = normals;
        gameObject.AddComponent<MeshCollider>().sharedMesh = mesh;
        print(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
