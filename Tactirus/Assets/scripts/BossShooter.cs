﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossShooter : MonoBehaviour
{
    public GameObject projectile;
    public float fireRate;

    private float nextFire;
    private bool isFocusingPlayer;
    public void focusPlayer() { isFocusingPlayer = true; }
    // Start is called before the first frame update
    void Start()
    {
        isFocusingPlayer = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFocusingPlayer && Time.time > nextFire)
        {
            print("fired");
            nextFire = Time.time + fireRate;
            Instantiate(projectile, transform.position, transform.rotation);
        }
    }
}
