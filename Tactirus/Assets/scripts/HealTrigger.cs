﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        print("Entree dans le block");
        BuildVirus virus = other.GetComponentInParent<BuildVirus>();
        virus.takeDamage(-2);
    }
}
