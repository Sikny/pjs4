﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealItem : MonoBehaviour
{
    private void FixedUpdate()
    {
        gameObject.transform.Rotate(new Vector3(0, 1, 0));
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            print("player healed");
            BuildVirus virus = other.GetComponentInParent<BuildVirus>();
            virus.takeDamage(-GameObject.Find("Virus").GetComponent<VirusStats>().MaxHealth() * 40 / 100);
            Destroy(gameObject);
        }
    }
}
