﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildFinalBoss : MonoBehaviour
{
    public GameObject prefabAround;
    public GameObject prefabPylon;
    public Vector3[] pylonsPositions = new Vector3[4];
    public Material virusMaterial;
    public GameObject arrivalPlatform;

    internal int destroyedPylons;
    private GameObject[] aroundElements;

    private bool[] remainingLife;

    private bool retracted;

    private float rotationSpeed = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        aroundElements = new GameObject[500];

        Vector3 parentPos = this.transform.position;
        Vector3 parentScale = this.transform.GetChild(0).transform.localScale;
        for(int i = 0; i < aroundElements.Length; i++)
        {
            aroundElements[i] = Instantiate(prefabAround);
            aroundElements[i].tag = "Boss";
            aroundElements[i].transform.SetParent(this.transform);
            aroundElements[i].transform.localScale = this.transform.localScale / 2;
            Vector3 newPos = parentPos + UnityEngine.Random.onUnitSphere * parentScale.x;
            newPos.y += aroundElements[i].transform.lossyScale.y*2;
            aroundElements[i].transform.position = newPos;
        }

        remainingLife = new bool[3]{
            true, true, true
        };

        retracted = false;
        
        arrivalPlatform.GetComponent<Renderer>().enabled = false;
        arrivalPlatform.GetComponent<Collider>().enabled = false;
        for(int i = 0; i < arrivalPlatform.transform.childCount; i++)
        {
            arrivalPlatform.transform.GetChild(i).GetComponent<Renderer>().enabled = false;
        }
    }

    public void expand()
    {
        Vector3 parentPos = this.transform.position;
        Vector3 parentScale = this.transform.GetChild(0).transform.localScale;
        for (int i = 0; i < aroundElements.Length; i++)
        {
            Vector3 newPos = parentPos + UnityEngine.Random.onUnitSphere * 5 * parentScale.x;
            newPos.y += aroundElements[i].transform.lossyScale.y * 2;
            aroundElements[i].transform.position = newPos;
        }
        retracted = false;
    }

    public void retract()
    {
        Vector3 parentPos = this.transform.position;
        Vector3 parentScale = this.transform.GetChild(0).transform.localScale;
        for (int i = 0; i < aroundElements.Length; i++)
        {
            Vector3 newPos = parentPos + UnityEngine.Random.onUnitSphere * parentScale.x;
            newPos.y += aroundElements[i].transform.lossyScale.y * 2;
            aroundElements[i].transform.position = newPos;
        }
        retracted = true;
    }

    void spawnPylons()
    {
        destroyedPylons = 0;
        for(int i = 0; i < pylonsPositions.Length; i++)
        {
            GameObject pylon = Instantiate(prefabPylon);
            pylon.transform.position = pylonsPositions[i];
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, rotationSpeed, 0));

        // Applying material depending on health
        int conversion = 500 / GetComponent<Breakable>().MAX_HEALTH;
        conversion = conversion * (GetComponent<Breakable>().MAX_HEALTH - GetComponent<Breakable>().getHealth());
        for (int i = 0; i < conversion; i++)
        {
            aroundElements[i].GetComponent<Renderer>().material = virusMaterial;
        }

        // Heals appears if player health equals 1
        if (GameObject.Find("Virus").GetComponent<VirusStats>().Health() == 1
            && !GameObject.Find("Heals").GetComponent<HealsData>().healsActive())
            GameObject.Find("Heals").GetComponent<HealsData>().spawnHeals();

        if(remainingLife[2] && GetComponent<Breakable>().getHealth() <= GetComponent<Breakable>().MAX_HEALTH * 0.75) {
            remainingLife[2] = false;

            print("La base virale a été mise à jour");
            // TODO : Alerte

            new WaitForSecondsRealtime(3);
            retract();
            GetComponent<BossShooter>().fireRate = 0.8f;
            spawnPylons();
        } else if(remainingLife[1] && GetComponent<Breakable>().getHealth() <= GetComponent<Breakable>().MAX_HEALTH * 0.5) {
            remainingLife[1] = false;

            print("La base virale a été mise à jour");
            // TODO : Alerte

            new WaitForSecondsRealtime(2);
            retract();
            GetComponent<BossShooter>().fireRate = 0.6f;
            spawnPylons();
        } else if(remainingLife[0] && GetComponent<Breakable>().getHealth() <= GetComponent<Breakable>().MAX_HEALTH * 0.25) {
            remainingLife[0] = false;

            print("La base virale a été mise à jour");
            // TODO : Alerte

            new WaitForSecondsRealtime(1);
            retract();
            GetComponent<BossShooter>().fireRate = 0.4f;
            spawnPylons();
        }

        // pylons destroyed
        if(destroyedPylons == 4 && retracted)
        {
            expand();
        }
    }

    private void OnDestroy()
    {
        arrivalPlatform.GetComponent<Renderer>().enabled = true;
        arrivalPlatform.GetComponent<Collider>().enabled = true;
        for (int i = 0; i < arrivalPlatform.transform.childCount; i++)
        {
            arrivalPlatform.transform.GetChild(i).GetComponent<Renderer>().enabled = true;
        }
    }
}
