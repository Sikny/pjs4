﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtons : MonoBehaviour
{
    void Start()
    {
        GameObject.Find("Virus").transform.Rotate(new Vector3(90, 0, 0));
    }

    public void exitBClicked() {
        Application.Quit();
    }

    public void newGameBClicked()
    {
        PlayerSaver.NewSave(0);
        UnityEngine.SceneManagement.SceneManager.LoadScene("level1");
    }

    public void loadGameBClicked()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(PlayerSaver.LoadStatsIn(new VirusStats(), 0), 0);
    }

    public void optionsBClicked()
    {

    }
}
