﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstFrameUpdate : MonoBehaviour
{
    void Update()
    {
        firstFrameUpdate();
        Destroy(this);
    }

    public void firstFrameUpdate()
    {
        GameObject.Find("Virus").GetComponent<BuildVirus>().takeDamage(0);
    }
}