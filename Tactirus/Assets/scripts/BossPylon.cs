﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPylon : Breakable
{
    override
    protected void breakObject()
    {
        GameObject.Find("Avost").GetComponent<BuildFinalBoss>().destroyedPylons++;
        print("pylons destroyed : " + GameObject.Find("Avost").GetComponent<BuildFinalBoss>().destroyedPylons);
        base.breakObject();
    }
}
