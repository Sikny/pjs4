﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpener : MonoBehaviour
{
    private bool state; // false : not activated, true : activated
    public bool OpenedState
    {
        get { return state; }
    }

    public Material activated, notActivated;

    private void Start()
    {
        state = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Virus" && Input.GetKeyDown(KeyCode.A))
        {
            print("A pressed");
            new WaitForSecondsRealtime(1);
            this.changeState();
        }
    }

    public void changeState()
    {
        state = !state;
        Transform movingCube = this.transform.Find("moving_cube");
        if (state) {
            movingCube.Translate(new Vector3(4, 0, 0));
            movingCube.gameObject.GetComponent<Renderer>().material = activated;
        } else {
            movingCube.Translate(new Vector3(-4, 0, 0));
            movingCube.gameObject.GetComponent<Renderer>().material = notActivated;
        }
    }
}
