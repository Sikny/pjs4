﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Class for breakable objects or damageable enemies
 */
public class Breakable : MonoBehaviour
{
    public GameObject onDestroyEffect;

    public int MAX_HEALTH;
    private int HEALTH;

    public int getHealth() { return HEALTH; }

    // Start is called before the first frame update
    void Start()
    {
        HEALTH = MAX_HEALTH;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected virtual void breakObject(){
        gameObject.GetComponent<Renderer>().enabled = false;
        GameObject instance = Instantiate(onDestroyEffect, this.transform.position, this.transform.rotation);
        instance.GetComponent<ParticleSystem>().Play();
        Destroy(gameObject);
    }

    public void takeDmg(int value)
    {
        HEALTH -= value;
        print("Health : " + HEALTH);
        if (HEALTH == 0)
            breakObject();
    }
}
