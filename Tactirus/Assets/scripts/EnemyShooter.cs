﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooter : MonoBehaviour
{
    public float shootingRange;
    public GameObject projectile;
    public float fireRate;

    private float nextFire;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<EnemyPathfinder>().isChasing()
            && Vector3.Distance(transform.position, GameObject.Find("Virus").transform.position) < shootingRange
            && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(projectile, transform.position, transform.rotation);
            print("fired");
        }
    }
}
