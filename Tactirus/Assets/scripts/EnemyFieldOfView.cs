﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFieldOfView : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.Equals(GameObject.Find("Virus").GetComponent<Collider>()))
        {
            print("Virus detected");
            this.GetComponent<EnemyPathfinder>().Invoke("chasePlayer", 0.1f);
        }
    }
}
