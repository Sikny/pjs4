﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealsData : MonoBehaviour
{
    public GameObject healPrefab;
    public Vector3[] spawnPositions = new Vector3[4];

    private void Update()
    {
        if (GameObject.Find("Virus").GetComponent<VirusStats>().Health() > 1
                && transform.childCount > 0)
            destroyHeals();
    }

    public void spawnHeals()
    {
        GameObject healTmp;
        for (int i = 0; i < spawnPositions.Length; i++) {
            healTmp = Instantiate(healPrefab, spawnPositions[i], Quaternion.identity);
            healTmp.transform.SetParent(transform);
        }
    }

    public void destroyHeals()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public bool healsActive()
    {
        return transform.childCount > 0;
    }
}
