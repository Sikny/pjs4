﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * this is supposed to be atteched to the camera
 */
public class InvisibleFacingWall : MonoBehaviour
{
    public GameObject playerCentered;   // center between camera should make walls invisible

    private RaycastHit[] hits = null;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (hits != null)
        {
            foreach (RaycastHit hit in hits)
            {
                Renderer r = hit.collider.GetComponent<Renderer>();
                if (r)
                {
                    r.enabled = true;
                }
            }
        }

        Debug.DrawRay(this.transform.position, (this.playerCentered.transform.position - this.transform.position), Color.magenta);

        hits = Physics.RaycastAll(playerCentered.transform.position, -(this.playerCentered.transform.position - this.transform.position),
            Mathf.Infinity);

        foreach (RaycastHit hit in hits)
        {
            Renderer r = hit.collider.GetComponent<Renderer>();
            if (r != null)
            {
                string material_name = r.material.name.Replace("(Instance)", "").Replace(" ", "");
                if (r && !(material_name == "virus_material" || material_name == "frame_col"))
                {
                    r.enabled = false;
                }
            }
        }
    }
}
