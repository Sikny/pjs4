﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    public Vector3 spawnPlayer;
    public string nextLevelName;
    public Material groundMaterial;
    private bool doColor;
    private float difference;
    private Color initialColor;

    internal void Start()
    {
        initialColor = groundMaterial.color;
        difference = 0;
        doColor = false;
        GameObject.Find("Virus").transform.position = spawnPlayer;
        GameObject.Find("Virus").GetComponent<PlayerController>().yOnStart = spawnPlayer.y;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals("Player"))
            doColor = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (doColor)
        {
            difference+=0.01f;
            groundMaterial.SetColor("_Color", new Color(initialColor.r, initialColor.g - difference,
                initialColor.b - difference));
            Debug.Log(groundMaterial.color);
        }
        if (difference >= 0.9)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(nextLevelName);
            PlayerSaver.SaveStats(GameObject.Find("Virus").GetComponent<VirusStats>(), 0, nextLevelName);
            groundMaterial.SetColor("_Color", initialColor);
            if (nextLevelName.Equals("mainMenu"))
            {
                Destroy(GameObject.Find("Virus"));
            }
        }
    }
}
