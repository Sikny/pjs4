﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]

public class VirusStats : MonoBehaviour
{
    public static VirusStats instance;

    public GameObject onDestroyEffect;

    private int maxHealth;
    public int MaxHealth() { return maxHealth; }
    public void setMaxHealth(int value) { maxHealth = value; }

    private int health;
    public int Health() { return health; }
    public void setHealth(int value) { health = value; }

    private float speed;
    public float Speed() { return speed; }
    public void setSpeed(float value) { speed = value; }

    private int damage;
    public int Damage() { return damage; }
    public void setDamage(int value) { damage = value; }

    private float attackSpeed;
    public float AttackSpeed() { return attackSpeed; }
    public void setAttackSpeed(float value) { attackSpeed = value; }


    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
            PlayerSaver.LoadStatsIn(this, 0);
        } else if(instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        GameObject instanceOnDestroy = null;
        if(health == 0)
        {
            instanceOnDestroy = Instantiate(onDestroyEffect, this.transform.position, this.transform.rotation);
            instanceOnDestroy.GetComponent<ParticleSystem>().Play();
            float totalDuration = instanceOnDestroy.GetComponent<ParticleSystem>().main.duration +
                instanceOnDestroy.GetComponent<ParticleSystem>().main.startLifetime.constant;
            Destroy(instanceOnDestroy, totalDuration);
            health = -1;
            GetComponent<BuildVirus>().killPlayer();
            StartCoroutine(Waiter());
        }
    }

public IEnumerator Waiter()
{
    yield return new WaitForSeconds(2);
    Destroy(gameObject);
    UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
}

// negative value to heal
public void takeDamage(int value)
    {
        Slider health_bar = gameObject.transform.Find("ATH").transform.Find("Health_Bar").GetComponent<Slider>();
        if (this.health - value < 0)
            this.health = 0;
        else if (this.health - value > maxHealth)
            this.health = maxHealth;
        else this.health -= value;
        health_bar.value = health;
        health_bar.maxValue = maxHealth;
        print("new health : " + health);
    }
}
