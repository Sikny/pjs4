﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildMenuVirus : MonoBehaviour
{
    private float rotationSpeed = 1.5f;
    private GameObject virusNode;
    private GameObject[] aroundSquares;
    private GameObject[] rotatingObjects;

    // Start is called before the first frame update
    void Start()
    {
        virusNode = GameObject.CreatePrimitive(PrimitiveType.Cube);
        virusNode.name = "Noyau";
        virusNode.tag = "Player";
        virusNode.transform.localScale = this.transform.localScale;
        virusNode.transform.position = this.transform.position;
        virusNode.GetComponent<Renderer>().material = this.GetComponent<Renderer>().material;
        virusNode.transform.SetParent(this.transform);

        aroundSquares = new GameObject[49]; // hp = 0 => noyau
        int nbRot = 8;
        rotatingObjects = new GameObject[nbRot];
        for (int i = 0; i < nbRot; i++)
        {
            rotatingObjects[i] = new GameObject();
            rotatingObjects[i].name = "Rotation " + (i + 1);
            rotatingObjects[i].tag = "Player";
            rotatingObjects[i].transform.position = this.transform.position;
            rotatingObjects[i].transform.SetParent(this.transform);
        }

        // Move at a random position around virus
        Vector3 parentPos = this.transform.position;
        Vector3 parentScale = this.transform.localScale;

        for (int i = 0; i < 49; i++)
        {
            // Create square
            aroundSquares[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
            aroundSquares[i].tag = "Player";

            // Circular
            aroundSquares[i].transform.position = parentPos + UnityEngine.Random.onUnitSphere * 2 * parentScale.x;

            // Quadratic
            /*new Vector3(Random.Range(parentPos.x - 2*parentScale.x, parentPos.x + 2*parentScale.x),
                                                                Random.Range(parentPos.y - 2*parentScale.y, parentPos.y + 2*parentScale.y),
                                                                Random.Range(parentPos.z - 2*parentScale.z, parentPos.z + 2*parentScale.z));*/

            // Set scale
            aroundSquares[i].transform.localScale = this.transform.localScale / 3;

            // Set material
            aroundSquares[i].GetComponent<Renderer>().material = this.GetComponent<Renderer>().material;

            // Set parent for rotation direction
            aroundSquares[i].transform.SetParent(rotatingObjects[i * nbRot / 49].transform);
            aroundSquares[i].GetComponent<Collider>().enabled = false;
        }
    }

    public void killPlayer()
    {
        virusNode.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // rotations
        rotatingObjects[0].transform.Rotate(new Vector3(rotationSpeed, 0, rotationSpeed));
        rotatingObjects[1].transform.Rotate(new Vector3(-rotationSpeed, 0, -rotationSpeed));
        rotatingObjects[2].transform.Rotate(new Vector3(rotationSpeed, 0, 0));
        rotatingObjects[3].transform.Rotate(new Vector3(-rotationSpeed, 0, 0));
        rotatingObjects[4].transform.Rotate(new Vector3(0, 0, rotationSpeed));
        rotatingObjects[5].transform.Rotate(new Vector3(0, 0, -rotationSpeed));
        rotatingObjects[6].transform.Rotate(new Vector3(-rotationSpeed, 0, rotationSpeed));
        rotatingObjects[7].transform.Rotate(new Vector3(rotationSpeed, 0, -rotationSpeed));

    }
}
