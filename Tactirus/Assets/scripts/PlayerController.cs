﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour{
    private Rigidbody rb;

    public GameObject shot;

    private float nextFire;

    internal float yOnStart;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        yOnStart = this.transform.position.y;
    }

    void Update()
    {
        if(GetComponent<VirusStats>().Health() > 1 && Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + GetComponent<VirusStats>().AttackSpeed();
            var ray = FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition);

            RaycastHit hit = new RaycastHit();

            int layer_ground = LayerMask.GetMask("Ground");
            if (Physics.Raycast(ray, out hit, 50.0f, layer_ground))
            {
                print("ok");
                Instantiate(shot, transform.position, transform.rotation);
                GetComponent<BuildVirus>().takeDamage(1);
            }
        }

        transform.rotation = Quaternion.identity;
    }

    void FixedUpdate()
    {
        // movement
        if (GetComponent<VirusStats>().Health() > 0)
        {
            float moveH = Input.GetAxis("Horizontal");
            float moveV = Input.GetAxis("Vertical");
            Vector3 movement = new Vector3(moveH, 0.0f, moveV);
            rb.AddForce(movement * GetComponent<VirusStats>().Speed() * 100);
            rb.velocity = new Vector3(0, 0, 0);
            Vector3 newPos = this.transform.position;
            newPos.y = yOnStart;
            this.transform.position = newPos;
        }
    }
}