﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyPathfinder : MonoBehaviour
{
    public NavMeshAgent agent;
    private bool doChase;
    private Vector3 initialVelocity, initialAngularVelocity;
    public float rangeToStopChasing;

    public bool isChasing()
    {
        return doChase;
    }

    // Start is called before the first frame update
    private void Start()
    {
        initialVelocity = gameObject.GetComponent<Rigidbody>().velocity;
        initialAngularVelocity = gameObject.GetComponent<Rigidbody>().angularVelocity;
        doChase = false;
    }
    // Update is called once per frame
    void Update()
    {
        if(doChase)
            agent.SetDestination(GameObject.Find("Virus").transform.position);
        if (Vector3.Distance(agent.transform.position, GameObject.Find("Virus").transform.position) < rangeToStopChasing)
        {
            agent.isStopped = true;
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
        else if(doChase){
            chasePlayer();
        }
            
    }

    void chasePlayer()
    {
        doChase = true;
        print("chasing player");
        gameObject.GetComponent<Rigidbody>().velocity = initialVelocity;
        gameObject.GetComponent<Rigidbody>().angularVelocity = initialAngularVelocity;
        agent.isStopped = false;
    }
}