﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    private float projectileSpeed;
    public int damage;
    // Start is called before the first frame update
    void Start()
    {
        print("started");
        projectileSpeed = 20;
        transform.localScale = GameObject.FindGameObjectWithTag("EnemyShooter").transform.localScale / 3;
        transform.LookAt(GameObject.Find("Virus").transform);
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x, 0, transform.forward.z) * projectileSpeed;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        // Objets autorisés à traverser
        if (!other.gameObject.tag.Equals("EnemyShooter") && !other.isTrigger)
        {
            print(other);
            BuildVirus b;
            if ((b = other.GetComponent<BuildVirus>()) && other.GetComponent<VirusStats>().Health() != -1)
            {
                b.takeDamage(damage);
            }
            Debug.Log(other.gameObject.name);
            Destroy(this.gameObject);
        }
    }
}
