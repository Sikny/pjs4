﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedDoor : MonoBehaviour
{
    public GameObject lever;
    private bool unlocked;
    // Start is called before the first frame update
    void Start()
    {
        unlocked = false;
    }

    private void FixedUpdate()
    {
        bool openedState = lever.GetComponent<DoorOpener>().OpenedState;
        if (openedState != unlocked)
        {
            Transform movableDoor = this.transform.Find("door_movable");
            if (openedState) {
                movableDoor.Translate(new Vector3(0, 0.1f + -1 * movableDoor.lossyScale.y, 0));
            } else {

                movableDoor.Translate(new Vector3(0, 0.1f + 1 * movableDoor.lossyScale.y, 0));
            }
        }
        unlocked = openedState;
    }
}
