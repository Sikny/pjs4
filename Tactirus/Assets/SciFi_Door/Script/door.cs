using UnityEngine;
using System.Collections;

public class door : MonoBehaviour {
	public GameObject thedoor;

    void OnTriggerEnter(Collider obj)
    {
        if (!obj.gameObject.tag.Equals("Projectile"))
        {
            thedoor.GetComponent<Animation>().Play("open");
        }
    }

    void OnTriggerExit ( Collider obj  ){
        if (!obj.gameObject.tag.Equals("Projectile"))
        {
            thedoor.GetComponent<Animation>().Play("close");
        }
    }
}